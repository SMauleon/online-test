import json

# Read JSON data from file
with open("../master_nodelist.json", "r") as file:
    data = json.load(file)

# Extract links from data
links = data["links"]

# Modify links
for link in links:
    if link["type"] == "walk":
        link["weight"] *= 2
        print(
            f"Modified link between {link['source']} and {link['target']} with new weight {link['weight']}")

# Save modified data back to JSON file
with open("../master_nodelist.json", "w") as output_file:
    json.dump(data, output_file, indent=4)

print("Modification completed. Updated output saved to 'master_nodelist.json'.")
