import os
import json


def load_master_nodelist():
    with open("../master_nodelist.json", "r") as file:
        return json.load(file)


def save_master_nodelist(data):
    with open("../master_nodelist2.json", "w") as file:
        json.dump(data, file, indent=4)



master_nodelist = load_master_nodelist()
filtered_nodes = []
filtered_links = []

prefix = "fatima_tricycle_terminal_"

for node in master_nodelist["nodes"]:
    if node["id"].startswith(prefix):
        filtered_nodes.append(node)

for link in master_nodelist["links"]:
    if link["source"].startswith(prefix) and link["target"].startswith(prefix):
        filtered_links.append(link)

master_nodelist["nodes"] = filtered_nodes
master_nodelist["links"] = filtered_links
save_master_nodelist(master_nodelist)
