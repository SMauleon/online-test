function generateRouteInstructions(routeData) {
    let commutes = routeData.details.commutes;
    let instructionElements = [];
  
    commutes.forEach((commute) => {
      let instruction = document.createElement("div");
      instruction.className = "instructionStyle";
  
      let name = commute.name.replace(/-/g, " ");
      let iconName = "";
  
      // Remove 'terminal' if present in name
      if (name.endsWith("terminal")) {
        name = name.slice(0, -9);
        iconName = "tric.png"; // Use tric.png icon
      } else if (name == "walk" && index < commutes.length - 1) {
        let nextCommute = commutes[index + 1].name.replace(/-/g, " ");
        name += " to " + nextCommute;
        if (!nextCommute.endsWith("terminal")) {
          name += " Jeepney Loading Point";
        }
      } else {
        name += " Jeepney";
        iconName = "jeep.png"; // Use jeep.png icon
      }
  
      if (commute.name != "walk") {
        name = "Ride " + name;
      }
  
      // Capitalize the first letter
      name = name
        .split(" ")
        .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
        .join(" ");
  
      let icon = document.createElement("img");
      icon.src = `images/${iconName}`;
      icon.className = "iconStyle";
      instruction.appendChild(icon);
  
      let textDiv = document.createElement("div");
      textDiv.textContent = name;
      textDiv.className = "subDivStyle nameStyle"; // Added a custom class for the name
      instruction.appendChild(textDiv);
  
      let costDiv = document.createElement("div");
      costDiv.textContent = `₱${Math.ceil(commute.cost)}`;
      costDiv.className = "subDivStyle";
      instruction.appendChild(costDiv);
  
      let distanceDiv = document.createElement("div");
      distanceDiv.textContent = `${commute.distance.toFixed(1)} km`;
      distanceDiv.className = "subDivStyle noBorder";
      instruction.appendChild(distanceDiv);
  
      // Create the 'i' icon
      let infoButton = document.createElement("i");
      infoButton.className = "fa fa-info-circle infoButtonStyle";
      
      // Create a popup element
      let popup = document.createElement("div");
      popup.className = "popup";
      popup.textContent = "Signboard: SM Pala Pala"; // Replace with your desired text
      popup.style.display = "none"; // Initially hide the popup
  
      // Show the popup on hover
      infoButton.addEventListener("mouseover", () => {
        popup.style.display = "block";
      });
  
      // Hide the popup when the mouse leaves the icon
      infoButton.addEventListener("mouseout", () => {
        popup.style.display = "none";
      });
  
      // Add the 'i' icon to the last column
      let infoColumn = document.createElement("div");
      infoColumn.appendChild(infoButton);
      instruction.appendChild(infoColumn);
  
      // Append the popup to the instruction
      instruction.appendChild(popup);
  
      instructionElements.push(instruction);
    });
  
    return instructionElements;
  }
  